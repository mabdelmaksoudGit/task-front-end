import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {CoursesModel} from '../courses/courses.model';
import {ApisService} from '../apis.service';
import {CategoriesModel} from './categories.model';

@Injectable()
export class CategoriesService implements Resolve<any> {
  onListChange: BehaviorSubject<any>;
  data: any;

  constructor(
    private apis: ApisService,
  ) {
    // Set the defaults
    this.onListChange = new BehaviorSubject([]);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise<void>((resolve, reject) => {
      Promise.all([
        this.list()
      ]).then(
        ([]) => {
          resolve();
        },
        reject
      );
    });
  }

  list(): Promise<any> {
    return new Promise((resolve, reject) => {
        this.apis.categories().subscribe((response: any) => {
          this.data = response.data.map((item: any) => new CoursesModel(item));
          this.onListChange.next(this.data);
          resolve(this.data);
        }, reject);
      }
    );
  }

  filter(term: any): void {
    if (term !== '') {
      this.apis.searchCategories(term)
        .subscribe((response: any) => {
          this.data = response.data.map((item: any) => new CoursesModel(item));
          console.log(this.data);
          this.onListChange.next(this.data);
        });
    } else {
      this.list();
    }
  }
}
