import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoriesRoutingModule} from './categories-routing.module';
import {ListComponent} from './list/list.component';
import {AddComponent} from './add/add.component';
import {CategoriesService} from './categories.service';
import {DataTablesModule} from 'angular-datatables';
import {ReactiveFormsModule} from '@angular/forms';
import {EditComponent} from './edit/edit.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    EditComponent,
  ],
  providers: [
    CategoriesService
  ],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
  ]
})
export class CategoriesModule {
}
