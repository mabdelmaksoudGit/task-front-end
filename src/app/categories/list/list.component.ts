import {Component, OnInit} from '@angular/core';
import {CategoriesService} from '../categories.service';
import {MatDialog} from '@angular/material/dialog';
import {ApisService} from '../../apis.service';
import {DeleteDialogComponent} from '../../components/delete-dialog/delete-dialog.component';
import {EMPTY, Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {

  data: any;
  dialogRef: any;
  term$ = new Subject<string>();
  private searchSubscription: Subscription;

  constructor(
    private service: CategoriesService,
    private matDialog: MatDialog,
    private apis: ApisService,
  ) {
    this.searchSubscription = this.term$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(term => {
        this.service.filter(term);
        return EMPTY;
      })
    ).subscribe();
  }

  ngOnInit(): void {
    this.service.onListChange
      .subscribe(data => {
        this.data = data;
      });
  }

  openDeleteDialog(id: any): void {
    this.dialogRef = this.matDialog.open(DeleteDialogComponent, {
      maxWidth: '60vw',
    });
    this.dialogRef.afterClosed().subscribe((data: any) => {
      if (data && data.delete) {
        this.delete(id);
      }
    });
  }

  delete(id: any): void {
    this.apis.deleteCategory(id).subscribe((response: any) => {
      const index = this.data.findIndex((obj: any) => obj.id === id);
      if (index > -1) {
        this.data.splice(index, 1);
      }
    });
  }

  next(e: any): void {
    this.term$.next(e?.value);
  }

  changeStatus(id: any): void {
    const index = this.data.findIndex((obj: any) => obj.id === id);
    if (index > -1) {
      this.data[index].isActive = !this.data[index].isActive;
    }
    this.apis.categoryActivity(id, {is_active: this.data[index].isActive}).subscribe((response: any) => {
      console.log(response);
    });
  }

}
