export class CoursesModel {
  id: string;
  name: string;
  isActive: boolean;
  category: string;
  description: string;
  hours: string;
  level: string;
  rating: string;
  views: string;

  constructor(obj: any) {
    this.id = obj.id;
    this.name = obj.name;
    this.isActive = obj.is_active;
    this.category = obj.category;
    this.description = obj.description;
    this.hours = obj.hours;
    this.level = obj.level;
    this.rating = obj.rating;
    this.views = obj.views;
  }
}
