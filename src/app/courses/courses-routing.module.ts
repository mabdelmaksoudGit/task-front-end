import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {AddComponent} from './add/add.component';
import {EditComponent} from './edit/edit.component';
import {CoursesService} from './courses.service';

const routes: Routes = [
  {path: '', component: ListComponent, resolve: {data: CoursesService}},
  {path: 'add', component: AddComponent},
  {path: ':id/edit', component: EditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {
}
