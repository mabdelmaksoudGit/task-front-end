import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {ApisService} from '../../apis.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  form: FormGroup;
  categories: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private apis: ApisService,
  ) {
    this.form = this.initForm();
  }

  ngOnInit(): void {
    this.getCategories();
  }

  initForm(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      categoryId: new FormControl('1', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      hours: new FormControl('', [Validators.required]),
      level: new FormControl('beginner', [Validators.required]),
      isActive: new FormControl(false, []),
    });
  }

  initRequest(): any {
    return {
      name: this.form.value.name,
      category_id: this.form.value.categoryId,
      is_active: this.form.value.isActive,
      description: this.form.value.description,
      hours: this.form.value.hours,
      levels: this.form.value.level,
    };
  }

  getCategories(): void {
    this.apis.categories().subscribe((response: any) => {
      this.categories = response.data;
    });
  }

  add(): void {
    this.apis.addCourse(this.initRequest()).subscribe((response: any) => {
      this.router.navigate(['courses']);
    });
  }

}
